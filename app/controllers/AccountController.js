const AccountModel = require("../models/Account");
const ListDataModel = require("../models/ListData");
var ObjectId = require("mongoose").Types.ObjectId;
const jwt = require("jsonwebtoken");
const Pagination = require("../utils/Pagination");
const md5 = require("md5");
const mailer = require("../utils/Mailer");

const makeid = (length) => {
  var result = "";
  var characters = "0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};
class AccountController {
  async infoAccount(req, res) {
    try {
      const _id = req.user._id;
      const data = await AccountModel.findOne({ _id: _id });
      const course = await ListDataModel.find({ idAccount: _id });
      if (data) {
        return res.status(200).json({
          status: 200,
          success: true,
          message: "Login successfully",
          data: {
            fullName: data.fullName,
            username: data.username,
            role: data.role,
            sex: data.sex,
            course: course ? course : null,
          },
        });
      } else {
        console.log("abc");
      }
    } catch (error) {
      return res.status(500).json({
        message: "Server Error",
        success: false,
        status: 500,
      });
    }
  }

  async loginAccount(req, res) {
    try {
      let username = req.body.username;
      let password = md5(req.body.password);
      const checkLogin = await AccountModel.findOne({
        username: username,
        password: password,
      });
      if (checkLogin !== null) {
        let token = jwt.sign(
          {
            exp: Math.floor(Date.now() / 1000) + 60 * 4320,
            _id: checkLogin._id,
          },
          "password"
        );
        const data = {
          _id: checkLogin._id,
          fullName: checkLogin.fullName,
          idClass: checkLogin.idClass,
          nameClass: checkLogin.nameClass,
          address: checkLogin.address,
          role: checkLogin.role,
          username: checkLogin.username,
          gender: checkLogin.gender,
          token: token,
        };
        res.header("auth-token", token);
        return res.status(200).json({
          message: "Loggin successfully",
          success: true,
          status: 200,
          data: data,
        });
      } else {
        return res.status(400).json({
          message: "Loggin failed. Account or password does not match",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Server Error",
        success: false,
        status: 500,
      });
    }
  }

  async registerUser(req, res) {
    try {
      let fullName = req.body.fullName;
      let idClass = req.body.idClass;
      let nameClass = req.body.nameClass;
      let address = req.body.address;
      let role = req.body.role;
      let email = req.body.email;
      let username = req.body.username;
      let password = req.body.password ? md5(req.body.password) : null;
      let gender = parseInt(req.body.gender);
      if (!username || !password) {
        return res.status(402).json({
          status: 402,
          success: false,
          message: "Username or password is required",
        });
      }
      if (gender === 0 || gender === 1) {
        const checkUsername = await AccountModel.findOne({ username: username });
        const checkEmail = await AccountModel.findOne({ email: email });
        if(checkEmail){
          return res.status(402).json({
            status: 402,
            success: false,
            message: "This email has been used",
          });
        }

        const date = new Date();
        if (!checkUsername) {
          AccountModel.create({
            fullName: fullName,
            idClass: idClass,
            nameClass: nameClass,
            address: address,
            role: role,
            email: email,
            username: username,
            password: password,
            gender: gender,
            createDate: date,
          });
          return res.status(200).json({
            message: "Account created successfully",
            status: 200,
            success: true,
          });
        } else {
          return res.status(402).json({
            status: 402,
            success: false,
            message: "This account has been created",
          });
        }
      } else {
        return res.status(400).json({
          status: 400,
          success: false,
          message: "Input data gender is incorrect",
        });
      }
    } catch (error) {
      return res.status(500).json({
        status: 500,
        success: false,
        message: error,
      });
    }
  }

  async changeInforMation(req, res) {
    const { fullName, address, email, gender, idClass, nameClass, parentName, phoneNumber, phoneNumberParent } = req.body;
    try {
      const checkAccount = await AccountModel.findOne({ _id: req.user._id });
      const checkEmail = await AccountModel.findOne({ email: email });
      if(checkEmail){
        return res.status(402).json({
            status: 402,
            success: false,
            message: "This email has been used",
          });
      }
      if (checkAccount) {
        checkAccount.fullName = fullName || checkAccount.fullName;
        checkAccount.address = address || checkAccount.address;
        checkAccount.idClass = idClass || checkAccount.idClass;
        checkAccount.nameClass = nameClass || checkAccount.nameClass;
        checkAccount.phoneNumber = phoneNumber || checkAccount.phoneNumber;
        checkAccount.parentName = parentName || checkAccount.parentName;
        checkAccount.phoneNumberParent = phoneNumberParent || checkAccount.phoneNumberParent;
        checkAccount.email = email || checkAccount.email;
        checkAccount.gender = parseInt(gender) || checkAccount.gender;
        checkAccount.save();
        return res.status(200).json({
          message: "Change Information successfully",
          status: 200,
          success: true,
        });
      } else {
        return res.status(404).json({
          message: "Account not found",
          status: 404,
          success: false,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Server Error",
        status: 500,
        success: false,
      });
    }
  }

  async changePassword(req, res) {
    let idAccount = req.body.idAccount;
    let password = req.body.password;
    let newpassword = req.body.newpassword;
    try {
      const idUser = await AccountModel.findOne({ _id: idAccount });
      if (md5(password) === idUser.password) {
        idUser.password = md5(newpassword);
        idUser.save();
        return res.status(200).json({
          message: "Password change successfully",
          success: true,
          status: 200,
        });
      } else {
        return res.status(402).json({
          message: "Old password is incorrect",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever error",
        success: false,
        status: 500,
      });
    }
  }

  async resetPassword(req, res) {
    let idAccount = req.params.idAccount;
    const newpassword = makeid(6);
    try {
      const idUser = await AccountModel.findOne({ _id: idAccount });
      if (idUser) {
        idUser.password = md5(newpassword);
        idUser.save();
        return res.status(200).json({
          message: newpassword,
          success: true,
          status: 200,
        });
      } else {
        return res.status(402).json({
          message: "Không tìm thấy tài khoản này",
          success: false,
          status: 400,
        });
      }
    } catch {
      return res.status(500).json({
        message: "Sever error",
        success: false,
        status: 500,
      });
    }
  }


  forgotPassword(req, res) {
    const email = req.body.email;
    AccountModel.findOne({ email: email })
      .then((data) => {
        const tokenResetPassword = makeid(6);
        data.tokenResetPassword = tokenResetPassword;
        const sub = "Reset Password - Thực Tập Tốt Nghiệp";
        const htmlContent = `
        <!doctype html>
        <html lang="en-US">
        <head>
            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
            <title>Reset Password TTTN</title>
            <meta name="description" content="Reset Password Email Template.">
            <style type="text/css">
                a:hover {text-decoration: underline !important;}
            </style>
        </head>
        
        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
            <!--100% body table-->
            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                <tr>
                    <td>
                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                            align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="height:80px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                        style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                        <tr>
                                            <td style="height:40px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="padding:0 35px;">
                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Bạn có yêu cầu đặt lại mật khẩu</h1>
                                                <span
                                                    style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                    Bạn hãy sử dụng mã cấp ở dưới để reset mật khẩu.
                                                </p>
                                                <a href="javascript:void(0);"
                                                    style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">${tokenResetPassword}</a>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td style="height:40px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="height:80px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--/100% body table-->
        </body>
        
        </html>`;
        mailer.sendMail(email, sub, htmlContent);
        data.save();
        res.status(200).json({
          message: "Your email has been sent successfully",
          success: true,
          status: 200,
        });
      })
      .catch((err) => {
        res.status(404).json({
          message: "Can't search email",
          success: false,
          status: 404,
        });
      });
  }

  newPasswordAfterPassword(req, res) {
    const email = req.query.email;
    const newPassword = req.body.newPassword;
    const md5newPassword = md5(newPassword);
    const token = req.body.token;
    AccountModel.findOne({ email: email, tokenResetPassword: token })
      .then((data) => {
        if (data === null) {
          res.status(402).json({
            message: "Token không hợp lệ",
            success: false,
            status: 402,
          });
        } else {
          data.password = md5newPassword;
          data.tokenResetPassword = null;
          data.save();
          res.status(200).json({
            message: "Change password successfully",
            success: true,
            status: 200,
          });
        }
      })
      .catch((err) => {
        res.status(402).json({
          message: "Invalid token",
          success: false,
          status: 402,
        });
      });
  }

  async getTeacherWithName(req, res) {
    const keyWord = req.query.keyWord;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    let listTeacher;
    if (keyWord) {
      listTeacher = await AccountModel.find({
        role: "teacher",
        fullName: { $regex: keyWord },
      });
    } else {
      listTeacher = await AccountModel.find({ role: "teacher" });
    }
    //const listTeacher = await AccountModel.find({role: "teacher",{ fullName: { $regex: ".*" + keyWord + ".*" }}});
    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: listTeacher.length,
        page_size: page_size,
        page_number: page_number,
      },
      data: Pagination(listTeacher, page_size, page_number),
    });
  }

  async getStudent(req, res) {
    const keyWord = req.query.keyWord;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    let listStudent;
    if (keyWord) {
      listStudent = await AccountModel.find({
        role: "student",
        fullName: { $regex: keyWord },
      });
    } else {
      listStudent = await AccountModel.find({
        role: "student",
      });
    }
    //const listTeacher = await AccountModel.find({role: "teacher",{ fullName: { $regex: ".*" + keyWord + ".*" }}});
    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: listStudent.length,
        page_size: page_size,
        page_number: page_number,
      },
      data: Pagination(listStudent, page_size, page_number),
    });
  }
}
module.exports = new AccountController();
