const ClassModel = require("../models/Class");
const AccountModel = require("../models/Account");
const Pagination = require("../utils/Pagination");

class ClassController {
  async addClass(req, res) {
    const nameClass = req.body.nameClass;
    const lv = req.body.lv;
    try {
      if (nameClass.trim()) {
        ClassModel.create({
          nameClass: nameClass.trim(),
          lv: lv,
        });
        return res.status(200).json({
          message: "Add class successfully",
          success: true,
          status: 200,
        });
      } else {
        return res.status(400).json({
          message: "Must enter  the class name",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Server Error",
        success: true,
        status: 200,
      });
    }
  }

  async addStudentToClass(req, res) {
    const idClass = req.query.idClass;
    const idStudent = req.query.idStudent;
    try {
      const checkStudent = await AccountModel.findOne({ _id: idStudent });
      const checkClass = await ClassModel.findOne({ _id: idClass });
      console.log(checkStudent, checkClass);
      if (checkStudent && checkClass) {
        checkStudent.idClass = idClass;
        checkStudent.nameClass = checkClass.nameClass;
        checkStudent.save();
        return res.status(200).json({
          message: "Add student to class successfully",
          success: true,
          status: 200,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Server Error",
        success: false,
        status: 500,
      });
    }
  }

  async appointAHomeroomTeacher(req, res) {
    const idTeacher = req.query.idTeacher;
    try {
      const _findTeacher = await AccountModel.findOne({ _id: idTeacher });
      if(_findTeacher){
        return res.status(200).json({
          message: "Add student to class successfully",
          success: true,
          status: 200,
        });
      }
    } catch (error) {}
  }

  async deleteClass(req, res) {
    const idClass = req.params.idClass;
    try {
      const findClass = await ClassModel.findOne({ _id: idClass });
      if (findClass) {
        const findStudentInClass = await AccountModel.findOne({ idClass: idClass });
        if (!findStudentInClass) {
          ClassModel.remove({ _id: idClass }, function (err) {
            if (!err) {
              return res.status(200).json({
                message: "Delete class successfully",
                success: true,
                status: 200,
              });
            } else {
              return;
            }
          });
        } else {
          return res.status(400).json({
            message: "Có học sinh trong lớp này",
            success: false,
            status: 400,
          });
        }
      } else {
        return res.status(400).json({
          message: "Class not found",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever Error",
        success: false,
        status: 500,
      });
    }
  }

  async getStudentByIdClass(req, res) {
    const idClass = req.query.idClass;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    try {
      const checkClass = await AccountModel.find({ idClass: idClass }).select("-__v");
      res.status(200).json({
        success: true,
        status_code: 200,
        meta: {
          total: checkClass.length,
          page_size: page_size,
          page_number: page_number,
        },
        data: Pagination(checkClass, page_size, page_number),
      });
    } catch (err) {
      return res.status(500).json({
        message: "Server Error",
        success: false,
        status: 500,
      });
    }
  }

  async getAllClass(req, res) {
    try {
      const checkAllClass = await ClassModel.find({}).select("-__v");
      const page_size = parseInt(req.query.PageSize) || 500;
      const page_number = parseInt(req.query.PageIndex) || 1;
      res.status(200).json({
        success: true,
        status_code: 200,
        meta: {
          total: checkAllClass.length,
          page_size: page_size,
          page_number: page_number,
        },
        data: Pagination(checkAllClass, page_size, page_number),
      });
    } catch (err) {
      return res.status(500).json({
        message: "Server Error",
        success: false,
        status: 500,
      });
    }
  }
}

module.exports = new ClassController();
