const ExerciseModel = require("../models/Exercise");
const AnswerModel = require("../models/Answer");
const _ = require("lodash");
const CourseModel = require("../models/Course");
const ListDataModel = require("../models/ListData");
const Pagination = require("../utils/Pagination");

class GradeController {
  async getPointStudent(req, res) {
    const idAccount = req.query.idAccount;
    const idCourse = req.query.idCourse;

    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;

    try {
      const _findAnswer = await AnswerModel.find({ idCourse: idCourse, idAccount: idAccount });
      const _findExercise = await ExerciseModel.find({ idCourse: idCourse });

      let _listPoint = [];
      for (let item of _findExercise) {
        const _flagAnswer = _findAnswer.find((e) => e.idExercise === item._id);
        const itemObject = {
          idAccount: parseInt(idAccount),
          idCourse: idCourse,
          idExercise: item._id,
          titleExercise: item.titleExercise,
          idAnswer: _flagAnswer ? _flagAnswer._id : null,
          studyPoint: _flagAnswer ? _flagAnswer.studyPoint : null,
          isTextPoint: item.isTextPoint,
        };
        _listPoint.push(itemObject);
      }
      res.status(200).json({
        success: true,
        status_code: 200,
        data: _listPoint,
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
      });
    }
  }

  async getPointCourseForTeacher(req, res) {
    const idCourse = req.query.idCourse;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    try {
      //const _findExercise = await ExerciseModel.findOne({ _id: idExercise });
      const _findStudentOfCourse = await ListDataModel.find({ idCourse: idCourse });
      const _findExercise = await ExerciseModel.find({ idCourse: idCourse });
      const _pointOfStudent = [];
      for (let item of _findStudentOfCourse) {
        const _findAnswer = await AnswerModel.find({ idCourse: idCourse, idAccount: item.idAccount });
        const _objectItem = {
          idAccount: item.idAccount,
          fullName: item.fullName,
          idCourse: item.idCourse,
          nameCourse: item.nameCourse,
          data: _findAnswer,
        };
        if (_findStudentOfCourse.role === "teacher") {
          _pointOfStudent.push(_objectItem);
        }
      }
      res.status(200).json({
        success: true,
        status_code: 200,
        data: Pagination(_pointOfStudent, page_size, page_number),
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
      });
    }
  }
}
module.exports = new GradeController();
