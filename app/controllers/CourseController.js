const AccountModel = require("../models/Account");
const LectureModel = require("../models/Lecture");
const CourseModel = require("../models/Course");
const ExerciseModel = require("../models/Exercise");
const ListData = require("../models/ListData");
const { v4: uuidv4 } = require("uuid");
class CourseController {
  async openCourse(req, res) {
    try {
      let nameCourse = req.body.nameCourse;
      let idTeacher = parseInt(req.body.idTeacher);
      if (!nameCourse) {
        return res.status(403).json({
          message: "Need to enter course name",
          success: false,
          status: 403,
        });
      }
      if (!idTeacher) {
        return res.status(403).json({
          message: "Need to enter id teacher",
          success: false,
          status: 403,
        });
      }
      const findTeacher = await AccountModel.findOne({ _id: idTeacher });
      const courseCheck = await CourseModel.findOne({ nameCourse: nameCourse });
      if (!courseCheck && findTeacher) {
        const id = uuidv4();
        CourseModel.create({
          _id: id,
          nameCourse: nameCourse,
          idTeacher: parseInt(findTeacher._id),
          fullNameTeacher: findTeacher.fullName,
        });
        ListData.create({
          idAccount: parseInt(findTeacher._id),
          fullName: findTeacher.fullName,
          nameCourse: nameCourse,
          idCourse: id,
          role: "teacher",
        });
        return res.status(200).json({
          message: "Create successfully",
          success: true,
          status: 200,
        });
      } else {
        return res.status(400).json({
          message: "The course already exists or teacher not found",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever Error",
        success: false,
        status: 500,
      });
    }
  }

  async editCourse(req, res) {
    let nameCourse = req.body.nameCourse;
    let idCourse = req.body.idCourse;
    try {
      const findCourse = await CourseModel.findOne({ _id: idCourse });
      const findExerciseOfCourse = await ExerciseModel.find({ idCourse: idCourse });
      const findListDataOfCourse = await ListData.find({ idCourse: idCourse });

      if (findCourse) {
        findCourse.nameCourse = nameCourse;
        findCourse.save();
        for (let item of findExerciseOfCourse) {
          item.nameCourse = nameCourse;
          item.save();
        }
        for (let item of findListDataOfCourse) {
          item.nameCourse = nameCourse;
          item.save();
        }
        return res.status(200).json({
          message: "Change Name Course successfully",
          success: true,
          status: 200,
        });
      } else {
        return res.status(400).json({
          message: "Course not found",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Server Error",
        success: false,
        status: 500,
      });
    }
  }

  async deleteCourse(req, res) {
    let idCourse = req.params.idCourse;
    try {
      const findCourse = await CourseModel.findOne({ _id: idCourse });
      const findExerciseOfCourse = await ExerciseModel.find({ idCourse: idCourse });
      const findLectureOfCourse = await LectureModel.find({ idCourse: idCourse });

      if (findExerciseOfCourse.length > 0 || findLectureOfCourse.length > 0) {
        return res.status(400).json({
          message: "Không thể xóa, khóa học này đang trong quá trình giảng dạy",
          success: false,
          status: 400,
        });
      } else {
        ListData.remove({ idCourse: idCourse }, function (err) {
          if (!err) {
            console.log("xoa thanh cong");
          }
        });
        CourseModel.remove({ _id: idCourse }, function (err) {
          if (!err) {
            return res.status(200).json({
              message: "Xóa thành công",
              success: true,
              status: 200,
            });
          } else {
            return res.status(400).json({
              message: "Không tìm thấy khóa học",
              success: false,
              status: 400,
            });
          }
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Server Error",
        success: false,
        status: 500,
      });
    }
  }

  async addStd(req, res) {
    try {
      let idStudent = req.body.idStudent;
      let idCourse = req.body.idCourse;
      const checkStd = await AccountModel.findOne({ _id: idStudent });
      const checkCourse = await CourseModel.findOne({ _id: idCourse });
      const checkListData = await ListData.findOne({ idAccount: idStudent, idCourse: idCourse });
      if (!checkStd) {
        return res.status(403).json({
          message: "This student could not be found",
          success: false,
          status: 403,
        });
      }
      if (!checkCourse) {
        return res.status(403).json({
          message: "This course could not be found",
          success: false,
          status: 403,
        });
      }

      if(checkListData){
        return res.status(400).json({
          message: "Học sinh đã ở trong khóa này",
          success: false,
          status: 400,
        });
      }
      if (checkStd && checkCourse && !checkListData) {
        ListData.create({
          idAccount: idStudent,
          fullName: checkStd.fullName,
          idCourse: idCourse,
          role: "student",
          nameCourse: checkCourse.nameCourse,
        });
        return res.status(200).json({
          message: "Add student successfully",
          success: true,
          status: 200,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Thêm thất bại",
        success: false,
        status: 500,
      });
    }
  }

  async addClassToCourse(req, res) {
    const idClass = req.query.idClass;
    const idCourse = req.query.idCourse;

    try {
      if (!idClass && !idCourse) {
        return res.status(403).json({
          message: "This id course or id class could not be found",
          success: false,
          status: 403,
        });
      }
      const checkStudentClass = await AccountModel.find({ idClass: idClass });
      const checkCourse = await CourseModel.findOne({ _id: idCourse });
      for (let item of checkStudentClass) {
        const checkStudentInCourse = await ListData.findOne({
          idStudent: item._id,
        });
        if (!checkStudentInCourse) {
          ListData.create({
            idAccount: item._id,
            fullName: item.fullName,
            idCourse: idCourse,
            role: "student",
            nameCourse: checkCourse.nameCourse,
          });
        }
      }
      return res.status(200).json({
        message: "Add class To Course successfully",
        success: true,
        status: 200,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Thêm thất bại",
        success: false,
        status: 500,
      });
    }
  }

  async getCourse(req, res) {
    const keySearchNameCourse = req.query.keySearchNameCourse;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    const idAccount = parseInt(req.query.idAccount);
    var start = (page_number - 1) * page_size;
    var end = page_size * page_number;
    let getData;
    if (idAccount && keySearchNameCourse) {
      getData = await ListData.find({
        idAccount: idAccount,
        nameCourse: { $regex: keySearchNameCourse },
      }).select("-__v");
    } else if (idAccount) {
      getData = await ListData.find({
        idAccount: idAccount,
      }).select("-__v");
    } else if (keySearchNameCourse) {
      getData = await ListData.find({ nameCourse: { $regex: keySearchNameCourse } }).select("-__v");
    } else {
      getData = await ListData.find({}).select("-__v");
    }

    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: getData.length,
        page_size: page_size,
        page_number: page_number,
      },
      data: getData.slice(start, end),
    });
  } // params page_size && page_number

  async getCourseForAdmin(req, res) {
    const keySearchNameCourse = req.query.keySearchNameCourse;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    const idAccount = parseInt(req.query.idAccount);
    var start = (page_number - 1) * page_size;
    var end = page_size * page_number;
    let getData;
    if (idAccount && keySearchNameCourse) {
      getData = await ListData.find({
        idAccount: idAccount,
        role: "teacher",
        nameCourse: { $regex: keySearchNameCourse },
      }).select("-__v");
    } else if (idAccount) {
      getData = await ListData.find({
        idAccount: idAccount,
        role: "teacher",
      }).select("-__v");
    } else if (keySearchNameCourse) {
      getData = await ListData.find({ role: "teacher", nameCourse: { $regex: keySearchNameCourse } }).select("-__v");
    } else {
      getData = await ListData.find({ role: "teacher" }).select("-__v");
    }

    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: getData.length,
        page_size: page_size,
        page_number: page_number,
      },
      data: getData.slice(start, end),
    });
  } // params page_size && page_number

  async getMemberInCourse(req, res) {
    const keySearchName = req.query.keySearchName;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    const idCourse = req.params.idCourse;
    var start = (page_number - 1) * page_size;
    var end = page_size * page_number;
    try {
      let getData;
      if (keySearchName) {
        getData = await ListData.find({
          idCourse: idCourse,
          fullName: { $regex: keySearchName },
        }).select("-__v");
      } else {
        getData = await ListData.find({
          idCourse: idCourse,
        }).select("-__v");
      }
      res.status(200).json({
        success: true,
        status_code: 200,
        meta: {
          total: getData.length,
          page_size: page_size,
          page_number: page_number,
        },
        data: getData.slice(start, end),
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
        message: error,
      });
    }
  }
}
module.exports = new CourseController();
