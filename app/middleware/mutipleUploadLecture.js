const util = require("util");
const multer = require("multer");
const AccountModel = require("../models/Account");
var fs = require("fs");
let storage = multer.diskStorage({
  destination: async (req, file, callback) => {
    const { idCourse , idExercise } = req.body;
    let path;
    console.log(req.user);
    const checkUser = await AccountModel.findOne({ _id: req.user});
    console.log(checkUser);
    if (checkUser.role === 'admin' || checkUser.role == "teacher") {
      path = `${__dirname}/../../uploadResults/Lecture/${idCourse}/${req.user._id}`;
    }
    fs.mkdirSync(path, { recursive: true });
    return callback(null, path);
  },
  filename: (req, file, callback) => {
    let filename = `${Date.now()}-${file.originalname}`;
    filename = filename.replace(/\s/g, '');
    callback(null, filename);
  },
});
let uploadManyFiles = multer({ storage: storage }).array("files", 100);
let multipleUploadLecture = util.promisify(uploadManyFiles);
module.exports = multipleUploadLecture;
