const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const Answer = new Schema(
  {
    _id: {
      type: Number,
    },
    idAccount: {
      type: Number,
    },
    fullName: {
      type: String,
    },
    idCourse:{
      type: String,
    },
    idExercise: {
      type: Number,
    },
    isTextPoint:{
      type: Number,
    },
    fileUpload: {
      type: Array,
      default: [],
    },
    descriptionAnswer: {
      type: String,
      default: null,
    },
    studyPoint: {
      type: Number,
      default: null,
    },
    feedbackFromTeacher: {
      type: Object,
      default: null,
    },
    feedbackFromTeacherByImage: {
      type: Object,
      default: null,
    },
    createDate: {
      type: Date,
      default: Date.now(),
    },
    updateDate: {
      type: Date,
      default: null,
    },
  },
  {
    _id: false,
    collection: "Answer",
  }
);

Answer.plugin(AutoIncrement, { id: "Answer", inc_field: "_id" });

Answer.plugin(mongooseDelete, {
  deleteAt: true,
  overrideMethods: "all",
});
module.exports = mongoose.model("Answer", Answer);
