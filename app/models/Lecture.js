const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const Lecture = new Schema(
  {
    _id: {
      type: Number,
    },
    nameLecture: {
      type: String,
      default: null,
    },
    descriptionLecture: {
      type: String,
      default: null,
    },
    idCourse: {
      type: String,
    },
    fileUpload: {
      type: Array,
      default: [],
    },
    createDate: {
      type: Date,
    },
  },
  {
    _id: false,
    collection: "Lecture",
  }
);
Lecture.plugin(AutoIncrement, { id: "Lecture", inc_field: "_id" });
Lecture.plugin(mongooseDelete, {
  deleteAt: true,
  overrideMethods: "all",
});
module.exports = mongoose.model("Lecture", Lecture);
