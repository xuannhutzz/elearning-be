const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const ListData = new Schema(
  {
    _id: {
      type: Number,
    },
    idAccount: { type: Number },
    fullName: {
      type: String,
    },
    idCourse: {
      type: String,
    },
    nameCourse: { type: String },
    role: { type: String }, // 2 phan quyen gv va hs
    createDate: {
      type: Date,
      default: Date.now(),
    },
  },
  {
    _id: false,
    collection: "ListData",
  }
);
ListData.plugin(AutoIncrement, { id: "ListData", inc_field: "_id" });
ListData.plugin(mongooseDelete, {
  deleteAt: true,
  overrideMethods: "all",
});
module.exports = mongoose.model("ListData", ListData);
