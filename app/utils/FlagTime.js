const moment = require("moment");

const flagTime = (startTime, endTime) => {
  let now = new Date();
  if (!endTime) {
    if (moment(now).isAfter(startTime)) {
      return true;
    } else {
      return false;
    }
  } else {
    if (moment(now).isAfter(startTime) && moment(now).isBefore(endTime)) {
      return true;
    } else {
      return false;
    }
  }
};

module.exports = { flagTime };
