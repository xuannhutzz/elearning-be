const moment = require("moment");
const formatDatePost = (date) => {
  return moment(new Date(date)).format("YYYY/MM/DD hh:mm:ss");
};

const formatDatePostShort = (date) => {
  return moment(new Date(date)).format("YYYY/MM/DD");
};

const formatDateShort = (date) => {
  return moment(new Date(date)).format("DD/MM/YYYY");
};

const parseDateHourToString = (date) => {
  var datetext = date.toTimeString();
  datetext = datetext.split(" ")[0];
  return datetext;
};

const parseDateHour = (hour) => {
  let d = new Date();
  let [hours, minutes, seconds] = hour.split(":");
  d.setHours(+hours);
  d.setMinutes(minutes);
  d.setSeconds(seconds);
  return d;
};

const getDateOfWeek = (dateParams) => {
  let date = new Date(dateParams);
  date.setDate(date.getDate() - 1);
  date = moment(date);
  let dayOfWeekArrs = [];
  for (let index = 0; index <= 6; index++) {
    dayOfWeekArrs.push(
      moment(date)
        .day(1 + index)
        .format("YYYY/MM/DD")
    );
  }
  let firstDayOfWeek = new Date(moment(date).day(1));
  let lastDayOfWeek = new Date(moment(date).day(7));
  //let dates = [firstday, lastday];
  let res = {
    firstDay: firstDayOfWeek,
    lastDayOfWeek: lastDayOfWeek,
    dayOfWeekArrs: dayOfWeekArrs,
    dateRange: [firstDayOfWeek, lastDayOfWeek],
  };
  return res;
};

module.exports = {
  formatDatePost,
  formatDatePostShort,
  formatDateShort,
  parseDateHourToString,
};
