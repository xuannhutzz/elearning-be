const express = require("express");
const router = express.Router();
const checkToken = require("../auth/CheckToken");
const checkTeacher = require("../auth/CheckTeacher");
const exerciseController = require("../controllers/ExerciseController");
const mutipleUploadController = require("../controllers/multipleUploadController");

router.post("/AddExercise", checkToken, checkTeacher, mutipleUploadController.multipleUpload, exerciseController.addExerciseText);
router.put("/EditExercise/", checkToken, checkTeacher, mutipleUploadController.multipleUpload, exerciseController.editExerciseText); // chỉ cho chỉnh sửa text, sửa file se co Api rient
router.delete("/DeleteExercise/:idExercise", checkToken, checkTeacher, exerciseController.deleteExercise);
router.get("/GetExerciseByCourse/:idCourse", checkToken, exerciseController.getAllExerciseByIdCourse);
router.get("/GetInformationExercise/:idExercise", checkToken, exerciseController.getAllInformationExercise); // API get thong tin all Exercise
router.get("/GetExerciseByIdAccount/:idAccount", exerciseController.getExerciseByIdAccount);
// router.get(
//   "/GetInformationExercise/:idExercise",
//   exerciseController.getInformationExercise
// );
router.get("/GetGradeExercise", exerciseController.getGradeExercise); //get diem cho gv

module.exports = router;
