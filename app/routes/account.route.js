const express = require("express");
const router = express.Router();
const checkToken = require("../auth/CheckToken");
const checkAdmin = require("../auth/CheckAdmin");

const accountController = require("../controllers/AccountController");

router.post("/Login", accountController.loginAccount);
router.post("/Register", accountController.registerUser);
router.get("/Information", checkToken, accountController.infoAccount);
router.put("/ChangePassword", checkToken, accountController.changePassword);
router.put("/ChangeInformation", checkToken, accountController.changeInforMation);
router.put("/ResetPasswordForAdmin/:idAccount", checkToken, accountController.resetPassword);
router.post("/ForgotPassword", accountController.forgotPassword);
router.put("/NewPasswordAfterForgotPassword", accountController.newPasswordAfterPassword);

router.get("/GetTeacher", accountController.getTeacherWithName);
router.get("/GetStudent", accountController.getStudent);

module.exports = router;
