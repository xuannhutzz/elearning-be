const express = require("express");
const router = express.Router();
const checkToken = require("../auth/CheckToken");
const checkAdmin = require("../auth/CheckAdmin");
const courseController = require("../controllers/CourseController");
const gradeController = require("../controllers/GradeController");
router.post("/AddCourse", checkToken, checkAdmin, courseController.openCourse);
router.put("/EditCourse", checkToken, checkAdmin, courseController.editCourse);
router.post("/AddStudent", checkToken, checkAdmin, courseController.addStd);
router.post("/AddClassToCourse", checkToken, courseController.addClassToCourse);
router.delete("/DeleteCourse/:idCourse", checkToken, courseController.deleteCourse);
router.get("/GetCourse", courseController.getCourse); // GET THEO ID Account
router.get("/GetCourseForAdmin", courseController.getCourseForAdmin); // GET THEO ID Account

router.get("/GetMemberInCourse/:idCourse", courseController.getMemberInCourse); // GET THEO ID Course
router.get("/GetPointStudent", checkToken, gradeController.getPointStudent); // GET THEO ID Course
router.get("/GetPointCourseForTeacher", checkToken, gradeController.getPointCourseForTeacher); // GET THEO ID Course

module.exports = router;
