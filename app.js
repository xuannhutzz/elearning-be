require('dotenv').config();
const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const db = require("./config/connectDb.js");
const route = require("./app/routes");
const app = express();
var fs = require("fs");
const { formatDatePost } = require("./app/utils/FormatTime");
const { flagTime } = require("./app/utils/FlagTime");

var path = require("path");
var dir = path.join(__dirname, "public");
app.use(express.static(dir));

app.use(cors());
app.use(cookieParser());
//Parse application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
route(app);
db.connect();
// [USE] parse application/json
// [USE] Morgan
app.use(morgan("combined"));
// ip use
app.post("/api/", (req, res) => {
  const testJson = req.body.testJson;
  for(let item of testJson){
    console.log(item);
  }
  res.json(testJson);
});

app.get("*", function (req, res) {
  var mime = {
    html: "text/html",
    txt: "text/plain",
    css: "text/css",
    gif: "image/gif",
    jpg: "image/jpeg",
    png: "image/png",
    svg: "image/svg+xml",
    js: "application/javascript",
    json: "application/json",
  };

  var dir = path.join(__dirname);
  var file = path.join(dir, req.path.replace(/\/$/, "/index.html"));
  console.log(file);
  if (file.indexOf(dir + path.sep) !== 0) {
    return res.status(403).end("Forbidden");
  }
  var type = mime[path.extname(file).slice(1)] || "text/plain";
  var s = fs.createReadStream(file);
  s.on("open", function () {
    res.set("Content-Type", type);
    s.pipe(res);
  });
  s.on("error", function () {
    res.set("Content-Type", "text/plain");
    res.status(404).end("Not found");
  });
});

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Example app listening at http://%s:%s", host, port);
});
